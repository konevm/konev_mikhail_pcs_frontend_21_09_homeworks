import "./Catalog.css";
import Productlist from "../../components/ProductList/Productlist";
import ExtraProducts from "../../components/ExtraProducts/ExtraProducts";
import { useEffect } from "react";

function Catalog() {
  const PRO = [];
  const SLIM = [];
  useEffect(() => {
    document.title = "CAT ENERGY | Каталог";
  }, []);
  class Product {
    constructor(id, Img, Name, Mass, Taste, Price, Quantity) {
      this.id = id;
      this.Img = Img;
      this.Name = Name;
      this.Mass = Mass;
      this.Taste = Taste;
      this.Price = Price;
      this.Quantity = Quantity;
      this.onclick = onclick;
    }
  }
  const PRO500Chicken = new Product(
    "1",
    "/images/catalog-1-desktop1.svg",
    "Cat Energy PRO 500 г",
    "500 г",
    "Курица",
    "700 Р.",
    5,
    1
  );
  const PRO1000Chicken = new Product(
    "2",
    "/images/catalog-2-desktop1.svg",
    "Cat Energy PRO 1000 г",
    "1000 г",
    "Курица",
    "1000 Р.",
    5,
    1
  );
  const PRO500Fish = new Product(
    "3",
    "/images/catalog-3-desktop.svg",
    "Cat Energy PRO 500 г",
    "500 г",
    "Рыба",
    "700 Р.",
    5,
    1
  );
  const PRO1000Fish = new Product(
    "4",
    "/images/catalog-4-desktop1.svg",
    "Cat Energy PRO 500 г",
    "1000 г",
    "Рыба",
    "1000 Р.",
    5,
    1
  );
  const SLIM500Buckwheat = new Product(
    "5",
    "/images/catalog-5-desktop.svg",
    "Cat Energy slim 500 г",
    "500 г",
    "Гречка",
    "400 Р.",
    5,
    1
  );
  const SLIM1000Buckwheat = new Product(
    "6",
    "/images/catalog-6-desktop.svg",
    "Cat Energy slim 1000 г",
    "1000 г",
    "Гречка",
    "700 Р.",
    5,
    1
  );
  const SLIM500Rice = new Product(
    "7",
    "/images/catalog-7-desktop.svg",
    "Cat Energy slim 500 г",
    "500 г",
    "Рис",
    "500 Р.",
    5,
    1
  );
  const PLUG = new Product();
  PRO.push(PRO500Chicken);
  PRO.push(PRO1000Chicken);
  PRO.push(PRO500Fish);
  PRO.push(PRO1000Fish);
  SLIM.push(SLIM500Buckwheat);
  SLIM.push(SLIM1000Buckwheat);
  SLIM.push(SLIM500Rice);
  SLIM.push(PLUG);

  return (
    <div className="Goods">
      <h1 className="title">Каталог продукции</h1>
      <h2 className="title__extra" id="PRO">
        Каталог PRO
      </h2>
      <div className="products__wrapper">
        {PRO.map((item) => (
          <Productlist key={item.id} {...item} />
        ))}
      </div>
      <h2 className="title__extra" id="SLIM">
        Каталог SLIM
      </h2>
      <div className="products__wrapper">
        {SLIM.map((item) => (
          <Productlist key={item.id} {...item} />
        ))}
      </div>
      <h2 className="title__extra">Дополнительные товары</h2>

      <ExtraProducts />
    </div>
  );
}

export default Catalog;
