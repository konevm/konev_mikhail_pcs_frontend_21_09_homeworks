import "./Main.css";
import MainTitle from "../../components/MainTitle/MainTitle";
import Infoblock from "../../components/Infoblock/Infoblock";
import catSlim from "../../assets/Index/cat-slim.svg";
import catPro from "../../assets/Index/cat-pro.svg";
import HowItWorks from "../../components/HowItWorks/HowItWorks";
import iconAlarm from "../../assets/Index/icon_alarm.svg";
import iconEat from "../../assets/Index/icon_eat.svg";
import iconLeaf from "../../assets/Index/icon_leaf.svg";
import iconTub from "../../assets/Index/icon_tub.svg";
import LiveExample from "../../components/LiveExample/LiveExample";
import { useEffect } from "react";

function Main() {
  useEffect(() => {
    document.title = "CAT ENERGY | Главная";
  }, []);
  return (
    <div className="Main">
      <MainTitle />
      <div className="main__wrapper">
        <div className="infoblock__wrapper">
          <Infoblock
            title="похудение"
            image={catSlim}
            text="Ваш кот весит больше собаки и почти утратил способность 
        лазить по деревьям? Пора на диету! Cat Energy Slim поможет вашему питомцу сбросить лишний вес."
            linkText="каталог slim"
            linkTo="SLIM"
          />
          <Infoblock
            title="набор массы"
            image={catPro}
            text="Заработать авторитет среди дворовых котов и даже собак? Серия Cat Energy Pro поможет 
            вашему коту нарастить необходимые мышцы!"
            linkText="каталог pro"
            linkTo="PRO"
          />
        </div>
        <h2 className="howitworks">Как это работает</h2>
        <div className="main__how-works">
          <HowItWorks
            image={iconLeaf}
            text="Функциональное питание
          содержит только полезные
          питательные вещества."
            number="1"
          />
          <HowItWorks
            image={iconTub}
            text="Выпускается в виде порошка, 
            который нужно лишь залить 
            кипятком и готово."
            number="2"
          />
          <HowItWorks
            image={iconEat}
            text="Замените один-два приема 
            обычной еды на наше 
            функциональное питание."
            number="3"
          />
          <HowItWorks
            image={iconAlarm}
            text="Уже через месяц наслаждайтесь 
            изменениями к лучшему вашего питомца!"
            number="4"
          />
        </div>
      </div>
      <LiveExample />
    </div>
  );
}

export default Main;
