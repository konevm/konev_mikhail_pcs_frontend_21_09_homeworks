import "./ProgramForm.css";
import axios from "axios";
import { useState, useEffect } from "react";

const ProgramForm = () => {
  useEffect(() => {
    document.title = "CAT ENERGY | Подбор программы";
  }, []);
  const [items, setItems] = useState(null);
  useEffect(() => {
    axios
      .get("http://localhost:3000/ExtraProduct.json")
      .then((res) => setItems(res.data.products));
  }, []);
  return (
    <div className="programForm">
      <div className="programForm__wrapper">
        <h1 className="title">Подбор программы</h1>
        <p className="title_text">
          Заполните анкету, и мы подберем программу питания для вашего кота
        </p>
        <form className="form" action="#" method="get">
          <div className="form__catMain">
            <div className="form__input">
              <span className="input__name">ИМЯ:*</span>
              <input className="input__field" type="text" name="name" />
            </div>
            <div className="form__input">
              <span className="input__name">Вес (кг):*</span>
              <input className="input__field" type="text" name="weight" />
            </div>
            <div className="form__input">
              <span className="input__name">Возраст (лет):</span>
              <input className="input__field" type="text" name="age" />
            </div>
          </div>
          <div className="form__purpose">
            <label>
              <input type="radio" name="purpose" value="slim" /> похудение
            </label>
            <label>
              <input type="radio" name="purpose" value="pro" /> Набор массы
            </label>
            <label>
              <input type="radio" name="purpose" value="other" /> Не знаю (Нужен
              ваш совет)
            </label>
          </div>
          <h3 className="form__title-light">
            Контактные данные (владельца кота)
          </h3>
          <div className="form__catMain">
            <div className="form__input">
              <span className="email">E-mail:*</span>
              <input className="input__field" type="text" name="email" />
            </div>
            <div className="form__input">
              <span className="">Телефон:*</span>
              <input className="input__field" type="phone" name="phone" />
            </div>
          </div>
          <h3 className="form__title-light">Комментарий</h3>
          <textarea
            className="form__textarea"
            name="form__hobs"
            cols="30"
            rows="10"
            placeholder="Расскажите обо всех повАдках кота"
          ></textarea>
          <h3 className="form__title-light">Дополнительно</h3>
          <div className="form__checkboxes">
            {items &&
              items.map((i) => {
                return (
                  <>
                    <input type="checkbox" id={i.id} name={i.id} />
                    <label htmlFor={i.id}>{i.name}</label>
                  </>
                );
              })}
          </div>
          <input
            type="submit"
            value="отправить заявку"
            style={{
              width: "100%",
              height: "40px",
              boxSizing: "border-box",
              backgroundColor: "#68B738",
              color: "white",
              fontFamily: "Oswald",
              fontStyle: "normal",
              fontWeight: "normal",
              fontSize: "16px",
              lineHeight: "20px",
              textTransform: "uppercase",
              border: "none",
            }}
          />
        </form>
      </div>
    </div>
  );
};

export default ProgramForm;
