import CartItem from "../../components/CartItem/CartItem";
import "./Cart.css"
import { useSelector, useDispatch } from 'react-redux'




const Cart = () => {   
    const { items, totalPrice, totalCount } = useSelector((cart) => cart )
    const dispatch = useDispatch();
    const removeItemFromCart = (id) =>{
      dispatch({
        type: "REMOVE_FROM_CART",
        payload: id,
    })
    }
    return (
    <div className="cart">        
        <p>Всего заказов {totalCount} на сумму {totalPrice}</p>
      {items && items.map((i)=> <CartItem item={i} 
      key={i.id} removeItemFromCart= {removeItemFromCart}/>)}
       
    </div>
    )}



export default Cart;