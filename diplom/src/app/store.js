import { createStore } from "redux";
import cart from "./redusers/cart";

const store = createStore(cart);

export default store;
