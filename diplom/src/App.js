import "./App.css";
import Catalog from "./pages/Catalog/Catalog";
import { Routes, Route } from "react-router";
import Main from "./pages/Main/Main";
import { Layout } from "./components/Layout/Layout";
import Cart from "./pages/Cart/Cart";
import ProgramForm from "./pages/ProgramForm/ProgramForm";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<Main />} />
          <Route path="/catalog" element={<Catalog />} />
          <Route path="/cart" element={<Cart />} />
          <Route path="/consult" element={<ProgramForm />} />
        </Route>
      </Routes>
    </div>
  );
}

export default App;
