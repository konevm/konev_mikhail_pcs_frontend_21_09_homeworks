import "./Footer.css";

function Footer() {
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
    });
  };
  return (
    <div className="Footer">
      <img
        className="Footer__logo"
        src="/footer-logo2.svg"
        alt="Logo"
        onClick={scrollToTop}
      />
      <div className="Footer__mark"></div>
      <ul className="Socials">
        <li>
          <a
            href="https://vk.com"
            target="_blank"
            rel="noreferrer noopener nofollow"
          >
            {" "}
            <img src="/icon_vkontakte.svg" alt="VK.com"></img>
          </a>
        </li>
        <li>
          {" "}
          <a
            href="https://instagram.com"
            target="_blank"
            rel="noreferrer noopener nofollow"
          >
            {" "}
            <img src="/icon_instagram.svg" alt="Instagram"></img>
          </a>
        </li>
        <li>
          <a
            href="https://facebook.com"
            target="_blank"
            rel="noreferrer noopener nofollow"
          >
            {" "}
            <img src="/icon_facebook.svg" alt="FACEBOOK.com"></img>
          </a>
        </li>
      </ul>
      <div className="Footer__mark"></div>
      <span className="Footer__tm">Konev Mikhail 2021</span>
    </div>
  );
}

export default Footer;
