import "./Header.css";
import {Link, NavLink} from "react-router-dom";
import basket from "../../assets/basket.png"
import { useSelector } from "react-redux";
// import { useEffect } from "react";


function Header() {
  
  const updateVisible = () => {
    const chooseMenu = document.querySelector('.header__burger');
    const chooseMenuList = document.querySelector('.header__menu');
    chooseMenu.classList.toggle("_active");
    chooseMenuList.classList.toggle("_active");
  };
  const { totalCount } = useSelector((cart) => cart )
    
  return (
    <header className="header" >
      <div className="header-wrapper">
        <Link to="/"><div className="Header__logo"></div></Link>
        <img src="logo 2.svg" alt="Name" className="header__name" />
        <div className="header__burger" onClick={updateVisible}>
          <span></span>
        </div>
           <div className={totalCount === 0 ? "header__cart": "header__cart cart_visible" } >
             <NavLink to="cart" > <img src={basket} alt="Корзина" style={{width: "30px", height: "30px"}}/></NavLink></div>
      <ul class="header__menu" onClick={updateVisible} onMouseLeave={updateVisible}>
        <li className="menu__line"></li>        
        <li className="header__item main-link"><NavLink to="/" >ГЛАВНАЯ</NavLink></li>
        <li className="menu__line"></li>
        <li className="header__item catalog-link"><NavLink to="catalog" >КАТАЛОГ ПРОДУКЦИИ</NavLink></li>
        <li className="menu__line"></li>
        <li className="header__item other-link"><NavLink to="consult" >ПОДБОР ПРОГРАММЫ</NavLink></li>
      
      </ul>
      </div>       
    </header>
  );
}

export default Header;
