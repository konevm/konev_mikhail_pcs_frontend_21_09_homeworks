import { YMaps, Map, Placemark } from 'react-yandex-maps';
import Partnership from "../Partnership/Partnership";
import "./MyMap.css"
import mapPin from "../../assets/Index/map_pin.png"
const MyMap = () => {
  return (    
  <div className="map__wrapper">
  <Partnership />
  <YMaps>
    <div >      
      <Map className='map' defaultState={{ center: [59.938635, 30.323118], zoom: 17 }}>
      <Placemark modules={['geoObject.addon.balloon']}
      defaultGeometry={[59.938635, 30.323118]}
       options={{
        iconLayout: 'default#image',
        iconImageHref: `${mapPin}`,
        iconImageSize: [113, 106]
        
      }}
      />
      </Map>
    </div>
  </YMaps>
  </div>
)
  };
export default MyMap 