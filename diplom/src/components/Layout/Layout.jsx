import {  Outlet } from "react-router-dom";
import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import MyMap from "../MyMap/MyMap";

const Layout = () => {
    return (
    <>
    <Header/>
    <main>
        <Outlet/>        
    </main>
    <MyMap />
      <Footer />
    </>
)
}

export { Layout }