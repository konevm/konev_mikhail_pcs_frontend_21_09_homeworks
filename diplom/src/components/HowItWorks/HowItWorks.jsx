import "./HowItWorks.css";



const HowItWorks = (props) => {
    return (
        <div className="HowItWorks__wrapper">
        <div className="HowItWorks">
            <div className="HowItWorks__imagewrapper">
                <img className="HowItWorks__image" src={props.image} alt="icon" />
            </div>
            <div className="HowItWorks__text"> {props.text} </div>
            <span className="HowItWorks__number"> {props.number} </span>
        </div>
        </div>
    )
}

export default HowItWorks;
