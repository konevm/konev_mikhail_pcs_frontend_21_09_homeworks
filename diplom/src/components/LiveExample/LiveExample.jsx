import "./LiveExample.css"
import { useState } from "react"
import CatFat from "../../assets/Index/cat-fat.png"
import CatSkinny from "../../assets/Index/cat-skinny.png"

const LiveExample = () =>{
    function block (label, text) {
        return (
            <div className="block">
                <div className="block__text">{text}</div>
                <div className="block__label">{label}</div>
            </div>
        )        
    }
    const ScalingImage = () => {
        const [size, setSize] = useState(50);
        return (
            <div className="liveexample__cats">
                <div className="cats__wrapper">
                <img className="cat1" src={CatSkinny} alt="Your cat after"/>
                <div className="cat2__wrapper" style={{ width: `calc(${size}% - 60px)` }}>                
                <img className="cat2" src={CatFat} alt="Your cat before" /> </div> 
                <div className="slider__wrapper">
                    <span className="slider__text">было</span>
                        <div className="slider"><input className="catSlider" type="range" 
                        min="0" max="100" value={size} onChange={e => {
                        const { value } = e.target; setSize(parseInt(value, 10)); }}
                        />
                        <div className="slider-line" style={{ width: `calc(${size}% - 8px`}}></div>
                    </div>
                    <span className="slider__text">стало</span>
                </div>
                </div>
                             
            </div>
          
        )
    }
    return (
        <div className="LiveExample">
            <div className="liveexample__wrapper">
                <span className="liveexample__line"></span>
                <h2 className="liveexample__title">Живой пример</h2>
                <p className="liveexample__text">Борис сбросил 5 кг за 2 месяца, 
                просто заменив свой обычный корм на Cat Energy Slim. Отличный 
                результат без изнуряющих тренировок! При этом он не менял своих 
                привычек и по-прежнему спит по 16 часов в день. </p>
                <div className="block__wrapper">
                    {block("снижение веса", "5 кг")}
                    {block("затрачено времени", "60 дней")}
                    <p className="example_price">Затраты на питание: 15 000 РУБ. </p>
                </div>
                
            </div>  
            {ScalingImage()}         
                
            
        </div>
    )
}

export default LiveExample;