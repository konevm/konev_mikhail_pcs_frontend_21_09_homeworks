import styled from 'styled-components';





function CartItem({item, removeItemFromCart}) {
    const { name, price} = (item)    
   
    const CartItem = styled.div `
        width: 100%;
        border: 1px solid #CDCDCD;
        display: flex;
        flex-direction: row;
        align-items: center;
    `;
    const CartName = styled.div `
        font-size: 20px;
        text-align: left;
        padding-left: 12px;
        width: 50%;
        text-transform: uppercase;
        `;
    const CartPrice = styled.div `
        font-size: 20px;
        text-align: right;
        padding-right: 12px;
        `;
        const CartPackage = styled.div `
        font-size: 20px;
        text-align: right;
        padding-right: 12px;
        `;
    const CartRemover = styled.div `
        box-sizing: border-box;
        position: relative;
        width: 20px;
        height: 20px;
        margin: 0;
        padding:0;        
        ::before {
            content: '';
            display: block;
            position: absolute;            
            height: 3px;
            width: 100%;            
            background-color: black;
            top: 50%;
            transform: rotate(45deg) ;
        }
        ::after {
            content: '';
            display: block;
            position: absolute;            
            height: 3px;
            width: 100%;            
            background-color: black;
            top: 50%;
            transform: rotate(-45deg);
        }
        `;
    return (        
        <CartItem>
            <CartName >{name}</CartName>
            <CartPackage>{item.package}</CartPackage>
            <CartPrice>{price}</CartPrice>
         
            <CartRemover onClick={
                ()=>{removeItemFromCart(item.id)}}></CartRemover>
           
        </CartItem>
    )
}

export default CartItem