import "./Infoblock.css";
import arrow from "../../assets/Index/Arrow.svg";
import { Link } from "react-router-dom";

function Infoblock(props) {
    const linkTo = `/catalog#${props.linkTo}`;
    return(
    <div className="infoblock">
        <h3 className="infoblock__title"> {props.title} </h3>
        <div className="image__wrapper"> <img className="image" src={props.image} alt={props.title} /></div>
        <span className="infoblock__text"> {props.text} </span>
        <div className="line"></div>
        <Link className="infoblock__link" to={linkTo} > <span className="link_text" > {props.linkText} </span> 
        <img src={arrow} alt="arrow" /> </Link>


    </div>
 )
    }

export default Infoblock;