import "./ExtraProduct.css"
import Button from '../Button/Button';



const ExtraProduct = ({item , addProductToCart}) => {    
  
    return (
        <div className="extra-product__wrapper">
            <div className="extra-product__name">{item.name}</div>
            <div className="extra-product__package">{item.package}</div>
            <div className="extra-product__price">{item.price} p.</div>            
            <div className="extra-product__button-wrapper" onClick={
                ()=>addProductToCart(item)} ><Button  type="Green" text="Заказать"/></div>
            
        </div>
    )  
}

export default ExtraProduct