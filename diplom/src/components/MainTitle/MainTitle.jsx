import "./MainTitle.css"
import Can from "../../assets/Index/index-can-mobile1.svg";

import Button from "../../components/Button/Button";

const MainTitle = () => {
    return (
        <div className="MainTitle">
             <div className="cat__background">
                    <div className="bacground__color" />                    
                </div>                
            <div className="title__wrapper">               
                <h1 className="main__title">Функциональное питание для котов</h1>
                <h3 className="tagline">Занялся собой? Займись котом!</h3>                  
            </div>
            <div className="main__info">                         
                <img src={Can} alt="Can" className="can" />
                <div className="button_wrapper">
                <Button type="Green" text="подобрать программу" />
                </div>
            </div>
      </div>
    )
}

export default MainTitle;
