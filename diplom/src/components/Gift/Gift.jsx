import styled from 'styled-components';
import Img from '../../assets/Catalog/GiftLayer.svg'
import iconGift from '../../assets/Catalog/icon_gift.svg'

const GiftWrapper = styled.div `
    width: 100%;
    height: 290px;
    position: relative;
    @media screen and (min-width: 768px) {
        height: 200px;  
        margin-top: 68px;      
    }
    @media screen and (min-width: 1440px) {
        width: 25%;
        height: 100%;  
        margin-top: 0px; 
        margin-left: 60px;     
`
const GiftBackground = styled.div` 
    box-sizing: border-box;
    background: rgb(104 183 56 /0.85);   
    width: 100%;
    height: 100%;
    
    overflow: hidden; 
    position: absolute;
    @media screen and (min-width: 768px) {
        height: 200px;               
    }
    @media screen and (min-width: 1440px) {
        height: 100%;               
    }
`;
const Wrapper = styled.div`
    background: url(${Img}) 0 0/100% auto no-repeat;
    width: 100%;
    height: 100%;
    background-position: center;
    z-index: 2;
    display: flex;
    flex-flow: column;
    align-items: center;
    justify-content: center; 
    @media screen and (min-width: 768px) {
        flex-flow: row;               
    }
    @media screen and (min-width: 1440px) {
        flex-flow: column;               
    }
`
const GiftImg = styled.img`
    z-index: 3;
    @media screen and (min-width: 768px) {
        position: absolute;
        left: 77px;
        padding: 0;      
    }
    @media screen and (min-width: 1440px) {
        position: relative;
        left: 0;
        padding: 0;      
`
const P = styled.p `
font-family: Arial, sans-serif;
font-style: normal;
line-height: 20px;
font-size: 16px;
width: 161px;
color: white;
margin: 0;
padding-top: 48px;
z-index: 3;
@media screen and (min-width: 768px) {
    position: absolute;
    padding: 0 120px 0 0;
    text-align: left;
    width: 148px; 
    right: 0;      
}
@media screen and (min-width: 1440px) {
    position: relative;
    padding: 46px 0 0 0;
    text-align: center;
    width: 161px; 
    margin: 0 auto;      
}
`

function Gift() {
    return (
        <GiftWrapper>   
            <GiftBackground>            
            </GiftBackground>
            <Wrapper>
            <GiftImg src={iconGift} alt="gift" />
            <P>Закажите все и получите чехол для кота в подарок!</P>
            </Wrapper>
        </GiftWrapper>
    )
}

export default Gift