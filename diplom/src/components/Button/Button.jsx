import './Button.css';



function Button (props) {
    
 
   return (
    
        <div className={'button ' + props.type}  >{props.text}</div>
    
    )
    
}

export default Button