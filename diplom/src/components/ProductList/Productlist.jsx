import Button from "../Button/Button";
import "./ProductList.css";
import plus from "../../assets/Catalog/plus.svg"

function Productlist (item, key) {  
  function createProduct (params) {
    return ( 
    <div className="product">
         <div className="Product__img"
              style={{ backgroundImage: `url(${item.Img})` }}></div>
              <div className="product__name">{item.Name} </div>
              <ul className="ProductNames">
            <li className="Product__mass">Масса</li>
            <li className="Product__taste">Вкус</li>
            <li className="Product__price">Цена</li>
          </ul>
         <ul className="Product">
            <li className="Product__mass">{item.Mass} </li>
            <li className="Product__taste">{item.Taste} </li>
            <li className="Product__price" >{item.Price} </li>
          </ul>
          {item.Quantity !==0 ?   <Button className="Button" type="Green" text="Заказать" onClick="" /> 
          : <Button className="Button" type="Grey" text="нет в наличии" />}          
     </div>
    )
  }
  function createPlug(params) {
    return (
      <div className="product plug">
         <div className="Product__img plug" 
              style={{ backgroundImage: `url(${plus})` }}></div>
              <div className="product__name_plug">Показать еще 100500 товаров </div>              
              <div className="product__plug">На самом деле вкусов гораздо больше!</div>
          <Button className="Button" type="Grey" text="показать все" />         
     </div>
    )   
  }
  if (item.id && item.id !=="") {
 return   createProduct(item)
   }
   return createPlug()
}

export default Productlist