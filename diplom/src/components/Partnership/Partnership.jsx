import "./Partnership.css";

const Partnership = () => {
    return (
        <div className="partnership">
            <span className="text">приглашаем <br/>к сотрудничеству <br/>дилеров!</span>
            <span className="adress">ул. Большая<br/> Конюшенная, д. 19/8 <br/> Санкт-Петербург</span>
        </div>
    )
}

export default Partnership;
