import "./ExtraProducts.css"
import  axios  from "axios";
import { useState, useEffect } from "react";
import  ExtraProduct from "../ExtraProduct/ExtraProduct"
import Gift from "../Gift/Gift";
import { useDispatch } from "react-redux";


const ExtraProducts = () => {
    const dispatch = useDispatch();
    const addProductToCart = (item) => {
        dispatch({
            type: "ADD_TO_CART",
            payload: item
        })
    }
const [items, setItems] = useState(null);
useEffect(() => {
    axios.get("https://konev-mikhail-pcs-frontend-21-09-homeworks.vercel.app/public/ExtraProduct.json").then((res) => setItems(res.data.products) )
}, []);
     return (
        <div className="extraProducts">
            <div className="extraProducts__wrapper">
              {items && items.map((i)=>{
                  return <ExtraProduct item={i} key={i.id} addProductToCart={addProductToCart}/>})}
            </div>
            <Gift />
        </div>
    )
}

export default ExtraProducts;
